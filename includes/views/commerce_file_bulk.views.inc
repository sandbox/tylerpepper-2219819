<?php

/**
 * Provides views integration for files.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_file_bulk_views_data_alter(&$data) {
  // Set existing download limit field handler to new custom bulk handler.
  $data['commerce_license']['download_limit']['field']['handler'] = 'commerce_file_bulk_handler_field_download_limit';
}
