<?php

/**
 * @file
 * Contains commerce_file_bulk_handler_field_download_limit.
 */

/**
 * Displays the download limit for a file
 */
class commerce_file_bulk_handler_field_download_limit extends commerce_file_handler_field_download_limit {

  // Render a new download limit based on the quantity in the license line items.
  function render($values) {
    $license = entity_load_single('commerce_license', $values->license_id);
    $download_amount = (isset($license->download_amount) && !empty($license->download_amount))? $license->download_amount : 1;
    if (isset($values->commerce_file_download_count)) {
      $download_limit = variable_get('commerce_file_download_limit', 100);
      return $values->commerce_file_download_count . ' / ' . ($download_limit * $download_amount);
    }
  }

}
