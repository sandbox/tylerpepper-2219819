Extends Commerce File functionality to allow for quantity purchasing of files.

Features
--------
- Users can now add a quantity before purchasing a file which multiplies how many downloads they're allowed by the download limit.
- If the download limit is not set, it does not do anything.

Requirements
------------
- patch: https://drupal.org/files/issues/commerce_file-allow_multiple_active_licenses_per_file-2215357-11-7.x_2.x.patch
- patch: https://drupal.org/files/issues/commerce_file-add_hook_for_download_limit-7.x_2.x.patch

See https://drupal.org/sandbox/tylerpepper/2219819 for information on getting started.